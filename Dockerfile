FROM opensuse/leap:15.4

RUN zypper -n refresh \
 && zypper -n install --no-recommends \
    bzip2 \
    gcc-c++ \
    git \
    gzip \
    libstdc++-devel \
    lua-lmod \
    patterns-devel-base-devel_basis \
    python3 \
    tree \
    rsync \
 && zypper clean \
 && python3 -m ensurepip

# https://gitlab.gwdg.de/mpsd-cs/spack-environments/-/merge_requests/56
RUN PATCHDIR="$(mktemp -d)" \
 && cd "$PATCHDIR" \
 && cp "$(readlink -e /bin/bash)" bash.patched \
 && printf '5.4' > dist_version \
 && dd if=dist_version of=bash.patched obs=1 seek=866776 conv=notrunc \
 && [ 5 -eq $(./bash.patched -c 'echo "${BASH_VERSINFO[0]}"') ] || { echo "Binary patching unsuccessful"; false; } \
 && mv bash.patched "$(readlink -e /bin/bash)" \
 && cd - \
 && rm -rf "$PATCHDIR"

# https://gitlab.gwdg.de/mpsd-cs/spack-environments/-/merge_requests/57
RUN mkdir -p /lib/terminfo \
 && infocmp -D | xargs -I{} rsync -avPh {}/ /lib/terminfo/

# https://github.com/spack/spack/issues/19573
ENV CONFIG_SITE=""

RUN python3 -m venv ~/venv \
 && . ~/venv/bin/activate \
 && pip3 install --upgrade pip setuptools \
 && pip3 install -v git+https://gitlab.gwdg.de/henri.menke04/mpsd-software-manager@79e4743

RUN . ~/venv/bin/activate \
 && mkdir -p ~/mpsd-software \
 && cd ~/mpsd-software \
 && mpsd-software init \
 && mpsd-software prepare 23c  \
 && cd 23c/spack-environments \
 && git checkout test-mpcdf-compile \
 && mpsd-software install 23c foss2022a-serial-min

RUN cd ~/mpsd-software/23c && tree -L 4 .

CMD ["/bin/bash -l"]