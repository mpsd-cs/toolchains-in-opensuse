.PHONY: build
build:
	docker build -t toolchain-in-opensuse --progress=plain  -f Dockerfile . 2>&1 | tee build.log